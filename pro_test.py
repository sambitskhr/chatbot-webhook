from aiokafka import AIOKafkaProducer
import asyncio

loop = asyncio.get_event_loop()

async def send_one():
    producer = AIOKafkaProducer(
        loop=loop, bootstrap_servers='velomobile-01.srvs.cloudkafka.com:9094,velomobile-02.srvs.cloudkafka.com:9094,velomobile-03.srvs.cloudkafka.com:9094',
        request_timeout_ms=6000, security_protocol='SASL_SSL', sasl_mechanism='PLAIN',
        sasl_plain_username='7g8dv4zk', sasl_plain_password='kTqt65pPZoS4uEXIFfcNr2kmu5S3Mh_b')
    # Get cluster layout and initial topic/partition leadership information
    await producer.start()
    try:
        # Produce message
        await producer.send_and_wait("7g8dv4zk-default", b"Super message")
    finally:
        # Wait for all pending messages to be delivered or expire.
        await producer.stop()

loop.run_until_complete(send_one())
